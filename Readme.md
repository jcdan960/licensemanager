to be able to push it:

`sudo docker build . -t jcdan3/lmrepo:lmtag`

`sudo docker push jcdan3/lmrepo:lmtag`

to pull in linode:
`docker login`

`docker pull jcdan3/lmrepo:v1`

to list available images:
`docker images`

to run in linode (139.177.199.117) and always restart by it self:

`sudo docker run -d -p 80:8080 --name lm_test4 --restart unless-stopped jcdan3/lmrepo:v1`

To check if currently running

`docker ps`


To delete a container
`docker rm <container_name>`

To see the logs of a container
`docker logs <container_id>`

SSL generation:
`openssl req -new -newkey rsa:2048 -nodes -keyout lm.key -out lm.csr`

`openssl  x509  -req  -days 365 -sha256 -extfile v3.ext  -in lm.csr  -signkey lm.key  -out lm.crt`

To get proper SSL certificate:
`https://certbot.eff.org/instructions`