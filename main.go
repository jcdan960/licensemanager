package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"licensemanager/server"
	"licensemanager/settings"
	"os"
)

func main() {

	settingsPtr := flag.String("settings", "settings.json", "Path to the settings JSON file")
	debug := flag.Bool("debug", false, "Debug mode")
	flag.Parse()

	settings := settings.ReadSettings(*settingsPtr)

	if settings == nil {
		log.Fatal("Failed to load settings. System shutting down.")
		return
	}

	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	log.SetFormatter(Formatter)

	log.SetOutput( os.Stdout)

	if *debug{
		log.Info("System booting up in debug mode")
	}else{
		log.Info(("System booting up in debug mode"))
	}

	server.Main(settings)
	log.Info("System shutting down")
}
