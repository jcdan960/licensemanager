package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"licensemanager/server/license"
	"net/http"
)

func GetLicense(w http.ResponseWriter, r *http.Request){
	// UUID ex: b8dcf63a-ae5c-11eb-8529-0242ac130003
	vars := mux.Vars(r)
	licenseUUID := vars["UUID"]
	invalidID := "Invalid UUID sent"

	log.Infof( "Get license endpoint hit, requesting license %s", licenseUUID)

	license := makeMockLicense(licenseUUID)

	if license == nil {
		log.Error(invalidID)
		fmt.Fprintln(w, invalidID)
		return
	}

	if err:= json.NewEncoder(w).Encode(license); err != nil{
		log.Fatal("Error creating JSON object in GetLicense endpoint")
		panic(err)
	}
}

func makeMockLicense(ID string) *license.License{

	localID, err := uuid.Parse(ID)
	if err != nil {
		log.Error("Wrong UUID!")
		return nil;
	}

	lis := license.License{
		localID,
		true,
		"Palantir Technologies inc",
	}
	return &lis
}

func PostLicense(w http.ResponseWriter, r *http.Request){
	log.Info(w, "Post license endpoint hit")
}

func Index(w http.ResponseWriter, r *http.Request){
	log.Info("Endpoint Hit: Index")
	fmt.Fprintf(w,"Endpoint Hit: Index")
}

