package routes

import (
	"github.com/gorilla/mux"
	"net/http"
	handlers "licensemanager/server/handlers"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		handlers.Index,
	},
	Route{
		"GetLicense",
		"GET",
		"/GetLicense/{UUID}",
		handlers.GetLicense,
	},
	Route{
		"PostLicense",
		"POST",
		"/PostLicense",
		handlers.PostLicense,
	},
}