package license

import "github.com/google/uuid"

type License struct{
	UUID uuid.UUID`json:"UUID`
	IsValid bool`json:"isValid`
	Company string`json:"Company`
}
