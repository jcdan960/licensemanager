package server

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	routes "licensemanager/server/routes"
	"licensemanager/settings"
	"net/http"
)

var SettingsCopy settings.Settings

func handleRequests() {
	router := routes.NewRouter().StrictSlash(true)

	PortStr := fmt.Sprintf(":%d", SettingsCopy.Server.Port)

	log.Infof("Server listening on port %d", SettingsCopy.Server.Port)
	if SettingsCopy.Server.UseSSL{
		SSLSets := &SettingsCopy.Server.SSL
		log.Infof("Server accepting HTTP requests with SSL")
		log.Fatal(http.ListenAndServeTLS( PortStr, SSLSets.CRTFilePath, SSLSets.KeyFilePath, router))
	}else{
		log.Infof("Server accepting HTTP requests without SSL")
		log.Fatal(http.ListenAndServe(PortStr, router))
	}
}

func Main(set* settings.Settings) {
	SettingsCopy = *set
	handleRequests()
}

