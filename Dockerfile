FROM golang:alpine3.13

# Move to working directory /build
WORKDIR /build
# Copy and download dependency using go mod
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY settings.json /dist/settings.json

COPY SSL/lm.crt /dist/SSL/lm.crt
COPY SSL/lm.key /dist/SSL/lm.key

# Copy the code into the container
COPY . .

# Build the application
RUN go build -o main .

# Move to /dist directory as the place for resulting binary folder
WORKDIR /dist

# Copy binary from build to main folder
RUN cp /build/main .

EXPOSE 8080
CMD ["/dist/main"]
