package settings

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
)

type SyslogSettings struct {
	OutputFile string `json:"OutputFile"`
}

type SSLSettings struct {
	CRTFilePath string `json:"CRTFilePath"`
	KeyFilePath string `json:"KeyFilePath"`
}

type ServerSettings struct {
	Port uint16 `json:"Port"`
	UseSSL bool `json:"UseSSL"`
	SSL SSLSettings `json:"SSL"`
}

type Settings struct{
	Server ServerSettings `json:"Server"`
	Syslog SyslogSettings `json:"Syslog"`
}

func ReadSettings(settingsFile string) *Settings {

	file, err := ioutil.ReadFile(settingsFile)

	if err != nil {
		log.Error("Error loading settings file")
		log.Error(err)
		return nil
	}

	settings := Settings{}
	_ = json.Unmarshal([]byte(file), &settings)

	return &settings
}

